<?php
require __DIR__ . '/../vendor/autoload.php';
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

$dbconn = @pg_pconnect("host=127.0.0.1 port=5432 dbname=Test user=user_php password=123456"); // or die('Не удалось соединиться: ' . @pg_last_error());
if ($dbconn === false) {
    print "";
    exit(0);
}

$app = AppFactory::create();

$twig = Twig::create(__DIR__.'/../templates', ['cache' => false]);
$app->add(TwigMiddleware::create($app, $twig));


$app->get('/users', function ($request, $response) {
    global $dbconn;
    $query = 'SELECT * FROM customers';
    $result = pg_query($dbconn, $query) or die('Ошибка запроса: ' . pg_last_error());
    $arr = pg_fetch_all($result, PGSQL_ASSOC);
    $view = Twig::fromRequest($request);
    return $view->render($response, 'users.twig', [
        'users' => $arr
    ]);
})->setName('profile');

$app->get('/new_user', function ($request, $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'new_user.twig');
})->setName('profile');

$app->post('/post_new_user', function ($request, $response, $args) {
    $_input = $request->getParsedBody();
    $_firstname = $_input['firstname'];
    $_lastname = $_input['lastname'];
    $_email = $_input['email'];
    $_age = $_input['age'];
    global $dbconn;
    $query = "INSERT INTO customers (firstname, lastname, email, age)  VALUES ('".$_firstname."', '".$_lastname."', '".$_email."', ".$_age.");";
    $result = @pg_query($dbconn, $query); // or die('Ошибка запроса: ' . pg_last_error());
    $view = Twig::fromRequest($request);
    return $view->render($response, 'post_new_user.twig', [
        'res' => $result
    ]);
})->setName('profile');

$app->get('/delete_user', function ($request, $response, $args) {
    global $dbconn;
    $query = 'SELECT * FROM customers';
    $result = pg_query($dbconn, $query) or die('Ошибка запроса: ' . pg_last_error());
    $arr = pg_fetch_all($result, PGSQL_ASSOC);
    $view = Twig::fromRequest($request);
    return $view->render($response, 'delete_user.twig', [
        'users' => $arr
    ]);
})->setName('profile');

$app->post('/post_delete_user', function ($request, $response, $args) {
    $_input = $request->getParsedBody();
    $_id = $_input['id'];
    global $dbconn;
    $res = pg_delete($dbconn, 'customers', [
        'id' => $_id
    ]);
    $view = Twig::fromRequest($request);
    return $view->render($response, 'post_delete_user.twig', [
        'res' => $res
    ]);
})->setName('profile');

$app->get('/update_user', function ($request, $response, $args) {
    global $dbconn;
    $query = 'SELECT * FROM customers';
    $result = pg_query($dbconn, $query) or die('Ошибка запроса: ' . pg_last_error());
    $arr = pg_fetch_all($result, PGSQL_ASSOC);
    $view = Twig::fromRequest($request);
    return $view->render($response, 'update_user.twig', [
        'users' => $arr
    ]);
})->setName('profile');

$app->post('/post_update_user', function ($request, $response, $args) {
    $_input = $request->getParsedBody();
    $_id = $_input['id'];
    global $dbconn;
    $query = 'SELECT * FROM public.customers WHERE id='.$_id;
    $result = pg_query($dbconn, $query) or die('Ошибка запроса: ' . pg_last_error());
    $arr = pg_fetch_all($result, PGSQL_ASSOC);
    $view = Twig::fromRequest($request);
    return $view->render($response, 'post_update_user.twig', [
        'user' => $arr[0]
    ]);
})->setName('profile');

$app->post('/post2_update_user', function ($request, $response, $args) {
    $_input = $request->getParsedBody();
    $_id = $_input['id'];
    $_firstname = $_input['firstname'];
    $_lastname = $_input['lastname'];
    $_email = $_input['email'];
    $_age = $_input['age'];
    global $dbconn;
    $query = "UPDATE customers SET firstname='".$_firstname."', lastname='".$_lastname."', email='".$_email."', age=".$_age."WHERE id = ".$_id.";";
    $result = pg_query($dbconn, $query) or die('Ошибка запроса: ' . pg_last_error());
    $view = Twig::fromRequest($request);
    return $view->render($response, 'post2_update_user.twig', [
        'res' => $result
    ]);
})->setName('profile');

$app->run();