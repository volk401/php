<?php
require __DIR__ . '/../vendor/autoload.php';
use Psr\Http\Message\ResponseInterface as Response;
use Psr\Http\Message\ServerRequestInterface as Request;
use Slim\Factory\AppFactory;
use Slim\Views\Twig;
use Slim\Views\TwigMiddleware;

// Соединение, выбор базы данных
$dbconn = @pg_connect("host=127.0.0.1 port=5432 dbname=Test user=user_php password=123456");// or die('Не удалось соединиться: ' . @pg_last_error());
if ($dbconn === false) {
    print "";
    exit(0);
}
// var_dump($dbconn);
// $dbconn = pg_connect("host=127.0.0.1 port=5432 dbname=Test user=postgres password=2017");
// // Очистка результата
// pg_free_result($result);

// // Закрытие соединения
// pg_close($dbconn);
// Выполнение SQL-запроса
$query = 'SELECT * FROM customers';
$result = pg_query($query) or die('Ошибка запроса: ' . pg_last_error());

// Вывод результатов в HTML
// echo "<table>\n";
// while ($line = pg_fetch_array($result, null, PGSQL_ASSOC)) {
//     // echo "\t<tr>\n";
//     // foreach ($line as $col_value) {
//     //     echo "\t\t<td>$col_value</td>\n";
//     // }
//     // echo "\t</tr>\n";
//     error_log(print_r($line));
// }
// echo "</table>\n";

// // Очистка результата
// pg_free_result($result);

// // Закрытие соединения
// pg_close($dbconn);

$line = pg_fetch_all($result, PGSQL_ASSOC);
// var_dump($line);
/*
$loader = new \Twig\Loader\FilesystemLoader(__DIR__.'/../templates');
$twig = new \Twig\Environment($loader, [
    #'cache' => '/path/to/compilation_cache',
]);
echo $twig->render('index.twig', ['name' => 'Fabien']);
exit(0);
//*/
$app = AppFactory::create();

$twig = Twig::create(__DIR__.'/../templates', ['cache' => false]);
$app->add(TwigMiddleware::create($app, $twig));


$app->get('/', function ($request, $response) {
    $view = Twig::fromRequest($request);
    return $view->render($response, 'index.twig', $line);
})->setName('profile');

// Run app
$app->run();